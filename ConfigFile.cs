﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace files_merge
{
    /// <summary>
    /// 配置文件
    /// </summary>
    class ConfigFile
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public ConfigFile()
        {

        }
        /// <summary>
        /// 创建配置文件
        /// </summary>
        /// <param name="path"></param>
        public void Create(string path, byte paddingbyte, RowDataInfo[] rowdata)
        {
            // 初始化一个xml实例
            XmlDocument xml = new XmlDocument();
            // 创建文件头声明
            XmlDeclaration xmldecl = xml.CreateXmlDeclaration("1.0", "utf-8", null);
            xml.AppendChild(xmldecl);

            // 创建root组
            XmlElement root = xml.CreateElement("root");
            xml.AppendChild(root);

            // 创建配置组
            XmlElement element = xml.CreateElement("config");
            root.AppendChild(element);

            XmlElement element1 = xml.CreateElement("PaddingByte");
            element1.InnerText = paddingbyte.ToString("X2");
            element.AppendChild(element1);

            // 创建文件列表组
            element = xml.CreateElement("FilesList");
            root.AppendChild(element);

            foreach (RowDataInfo row in rowdata)
            {
                element1 = xml.CreateElement("File");
                element1.InnerText = Path.GetFileName(row.FileName);
                element1.SetAttribute("FileName", row.FileName);
                element1.SetAttribute("FileLength", row.FileLength.ToString());
                element1.SetAttribute("FileComment", row.FileComment);
                element1.SetAttribute("StartAddr", row.StartOffsetAddr.ToString());
                element1.SetAttribute("EndAddr", row.EndOffsetAddr.ToString());
                element.AppendChild(element1);
            }

            // 保存xml文件
            xml.Save(path);
        }

        /// <summary>
        /// 读取
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public ConfigFileArgs Read(string path)
        {
            // 配置文件参数
            ConfigFileArgs args = new ConfigFileArgs();
            // 初始化一个xml实例
            XmlDocument xml = new XmlDocument();

            try
            {
                // 加载文件
                xml.Load(path);

                XmlNode root = xml.SelectSingleNode("root");
                XmlNode config = root.SelectSingleNode("config");
                XmlNode file = root.SelectSingleNode("FilesList");

                XmlNodeList file_list = file.ChildNodes;

                XmlNode node = config.SelectSingleNode("PaddingByte");
                args.padByte = Convert.ToByte(node.InnerText, 16);

                args.rowsData = new RowDataInfo[file_list.Count];
                int count = 0;
                foreach (XmlNode file_node in file_list)
                {
                    XmlElement xe = (XmlElement)file_node;
                    args.rowsData[count].FileName = xe.GetAttribute("FileName");
                    args.rowsData[count].FileLength = Convert.ToUInt32(xe.GetAttribute("FileLength"));
                    args.rowsData[count].FileComment = xe.GetAttribute("FileComment");
                    args.rowsData[count].StartOffsetAddr = Convert.ToUInt32(xe.GetAttribute("StartAddr"));
                    args.rowsData[count].EndOffsetAddr = Convert.ToUInt32(xe.GetAttribute("EndAddr"));
                    count++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("XML文件操作失败，请确认文件是否正确\r\n" + ex.Message, "温馨提示", MessageBoxButtons.OK);
                
            }
            return args;
        }
    }

    /// <summary>
    /// 配置文件参数
    /// </summary>
    public struct ConfigFileArgs
    {
        public byte padByte;
        public RowDataInfo[] rowsData;
    }
}
