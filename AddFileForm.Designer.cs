﻿namespace files_merge
{
    partial class AddFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txbComment = new System.Windows.Forms.TextBox();
            this.skinLabel2 = new System.Windows.Forms.Label();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.skinLabel1 = new System.Windows.Forms.Label();
            this.txbFilePath = new System.Windows.Forms.TextBox();
            this.txbOffsetAddr = new System.Windows.Forms.TextBox();
            this.chkCustomAddr = new System.Windows.Forms.CheckBox();
            this.chkHexFormat = new System.Windows.Forms.CheckBox();
            this.lblFileSize = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(10, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 12);
            this.label1.TabIndex = 15;
            this.label1.Text = "文件描述（文件注释信息，可以留空）";
            // 
            // txbComment
            // 
            this.txbComment.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txbComment.Location = new System.Drawing.Point(12, 106);
            this.txbComment.Name = "txbComment";
            this.txbComment.Size = new System.Drawing.Size(332, 21);
            this.txbComment.TabIndex = 10;
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(10, 9);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(125, 12);
            this.skinLabel2.TabIndex = 13;
            this.skinLabel2.Text = "文件存放位置偏移地址";
            // 
            // btnConfirm
            // 
            this.btnConfirm.BackColor = System.Drawing.Color.Transparent;
            this.btnConfirm.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnConfirm.Location = new System.Drawing.Point(274, 133);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(70, 21);
            this.btnConfirm.TabIndex = 14;
            this.btnConfirm.Text = "确定";
            this.btnConfirm.UseVisualStyleBackColor = false;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.BackColor = System.Drawing.Color.Transparent;
            this.btnOpenFile.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnOpenFile.Location = new System.Drawing.Point(274, 64);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(70, 21);
            this.btnOpenFile.TabIndex = 8;
            this.btnOpenFile.Text = "浏览...";
            this.btnOpenFile.UseVisualStyleBackColor = false;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(10, 49);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(53, 12);
            this.skinLabel1.TabIndex = 11;
            this.skinLabel1.Text = "文件路径";
            // 
            // txbFilePath
            // 
            this.txbFilePath.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txbFilePath.Location = new System.Drawing.Point(12, 64);
            this.txbFilePath.Name = "txbFilePath";
            this.txbFilePath.ReadOnly = true;
            this.txbFilePath.Size = new System.Drawing.Size(256, 21);
            this.txbFilePath.TabIndex = 9;
            // 
            // txbOffsetAddr
            // 
            this.txbOffsetAddr.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txbOffsetAddr.Location = new System.Drawing.Point(93, 24);
            this.txbOffsetAddr.MaxLength = 10;
            this.txbOffsetAddr.Name = "txbOffsetAddr";
            this.txbOffsetAddr.Size = new System.Drawing.Size(175, 21);
            this.txbOffsetAddr.TabIndex = 12;
            this.txbOffsetAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txbOffsetAddr.TextChanged += new System.EventHandler(this.txbOffsetAddr_TextChanged);
            this.txbOffsetAddr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txbOffsetAddr_KeyPress);
            // 
            // chkCustomAddr
            // 
            this.chkCustomAddr.AutoSize = true;
            this.chkCustomAddr.Location = new System.Drawing.Point(12, 27);
            this.chkCustomAddr.Name = "chkCustomAddr";
            this.chkCustomAddr.Size = new System.Drawing.Size(84, 16);
            this.chkCustomAddr.TabIndex = 17;
            this.chkCustomAddr.Text = "自定义地址";
            this.chkCustomAddr.UseVisualStyleBackColor = true;
            this.chkCustomAddr.CheckedChanged += new System.EventHandler(this.chkCustomAddr_CheckedChanged);
            // 
            // chkHexFormat
            // 
            this.chkHexFormat.AutoSize = true;
            this.chkHexFormat.Location = new System.Drawing.Point(276, 27);
            this.chkHexFormat.Name = "chkHexFormat";
            this.chkHexFormat.Size = new System.Drawing.Size(66, 16);
            this.chkHexFormat.TabIndex = 18;
            this.chkHexFormat.Text = "HEX格式";
            this.chkHexFormat.UseVisualStyleBackColor = true;
            this.chkHexFormat.CheckedChanged += new System.EventHandler(this.chkHexFormat_CheckedChanged);
            // 
            // lblFileSize
            // 
            this.lblFileSize.AutoSize = true;
            this.lblFileSize.Location = new System.Drawing.Point(12, 137);
            this.lblFileSize.Name = "lblFileSize";
            this.lblFileSize.Size = new System.Drawing.Size(53, 12);
            this.lblFileSize.TabIndex = 19;
            this.lblFileSize.Text = "文件大小";
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnCancel.Location = new System.Drawing.Point(198, 133);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(70, 21);
            this.btnCancel.TabIndex = 20;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // AddFileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 161);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblFileSize);
            this.Controls.Add(this.txbOffsetAddr);
            this.Controls.Add(this.chkHexFormat);
            this.Controls.Add(this.chkCustomAddr);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txbComment);
            this.Controls.Add(this.skinLabel2);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.btnOpenFile);
            this.Controls.Add(this.skinLabel1);
            this.Controls.Add(this.txbFilePath);
            this.Name = "AddFileForm";
            this.Text = "添加文件";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbComment;
        private System.Windows.Forms.Label skinLabel2;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Label skinLabel1;
        private System.Windows.Forms.TextBox txbFilePath;
        private System.Windows.Forms.TextBox txbOffsetAddr;
        private System.Windows.Forms.CheckBox chkCustomAddr;
        private System.Windows.Forms.CheckBox chkHexFormat;
        private System.Windows.Forms.Label lblFileSize;
        private System.Windows.Forms.Button btnCancel;
    }
}